/**
 * Copyright (C) 2020  The Software Heritage developers
 * See the AUTHORS file at the top-level directory of this distribution
 * License: GNU Affero General Public License version 3, or any later version
 * See top-level LICENSE file for more information
 */

$(document).ready(function() {
  var params = new URLSearchParams(window.location.search);
  if (params.has('swh-web-user-profile')) {
    // simplify account UI and modify its style when embedding it in swh-web
    $('body, .bs-sidebar').css('background', 'white');
    $('h2').css('margin-top', '10px');
    $('h2').css('margin-bottom', '10px');
    $('.content-area').css('border-right', 'none');
    $('div.container').removeClass('container').addClass('container-fluid');
    // remove top navbar
    $('.navbar').remove();
    // remove Sessions and Applications menu entries
    $('.bs-sidebar li').each(function(idx, element) {
      if (idx >= 3) {
        $(element).remove();
      }
    });
  }
});
