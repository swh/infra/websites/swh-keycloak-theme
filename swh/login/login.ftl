<!--
  Copyright (C) 2020  The Software Heritage developers
  See the AUTHORS file at the top-level directory of this distribution
  License: GNU Affero General Public License version 3, or any later version
  See top-level LICENSE file for more information
  -->

<#import "template.ftl" as layout>
<#if !realm.loginWithEmailAllowed>
  <#assign usernamePlaceholder = msg("username")>
<#elseif !realm.registrationEmailAsUsername>
  <#assign usernamePlaceholder = msg("usernameOrEmail")>
<#else>
  <#assign usernamePlaceholder = msg("email")>
</#if>
<@layout.registrationLayout displayInfo=social.displayInfo; section>
  <#if section = "title">
    ${msg("loginTitle",(realm.displayName!''))}
  <#elseif section = "header">
    <div class="title">
      ${msg("loginTitleHtml",(realm.displayNameHtml!''))?no_esc}
    </div>
  <#elseif section = "form">
    <#if realm.password>
      <form id="kc-form-login" class="form ${properties.kcFormClass!}"
            action="${url.loginAction}" method="post">
        <div class="input-group mb-3">
          <#if usernameEditDisabled??>
            <input id="username" type="text" class="form-control ${properties.kcInputClass!}"
                   name="username" placeholder="${usernamePlaceholder}" disabled />
          <#else>
            <input id="username" type="text" class="form-control ${properties.kcInputClass!}"
                   name="username" placeholder="${usernamePlaceholder}"
                   autofocus autocomplete="off" />
          </#if>
          <div class="input-group-append">
            <div class="input-group-text">
              <#if !realm.loginWithEmailAllowed>
                <i class="mdi mdi-account"></i>
              <#elseif !realm.registrationEmailAsUsername>
                <i class="mdi mdi-account"></i>
                &nbsp;/&nbsp;
                <i class="mdi mdi-email"></i>
              <#else>
                <i class="mdi mdi-email"></i>
              </#if>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input id="password" type="password" class="form-control ${properties.kcInputClass!}"
                 placeholder="${msg("password")}" name="password" />
          <div class="input-group-append">
            <div class="input-group-text">
              <i class="mdi mdi-lock"></i>
            </div>
          </div>
        </div>
        <div id="kc-form-options" class="${properties.kcFormOptionsClass!} row">
          <div class="col-7">
            <#if realm.rememberMe && !usernameEditDisabled??>
              <div class="custom-control custom-checkbox">
                <#if login.rememberMe??>
                  <input class="custom-control-input" name="rememberMe" type="checkbox"
                          id="rememberMe" checked>
                <#else>
                  <input class="custom-control-input" name="rememberMe" type="checkbox"
                          id="rememberMe">
                </#if>
                <label class="custom-control-label" for="rememberMe">
                  ${msg("rememberMe")}
                </label>
              </div>
            </#if>
          </div>
          <div class="col-5">
            <button type="submit" class="btn btn-primary btn-block"
              name="login" id="kc-login" type="submit">${msg("doLogIn")}</button>
          </div>
        </div>
        <#if social.providers??>
          <div class="social-auth-links text-center mb-3">
            <p> - ${msg("or")} - </p>
            <div id="kc-social-providers">
              <#list social.providers as p>
                <a href="${p.loginUrl}" class="btn btn-block btn-primary social-provider">
                  <i class="mdi mdi-${p.alias} mr-2"></i>
                  ${msg("doLogIn")} ${msg("with")} ${p.displayName}
                </a>
              </#list>
            </div>
          </div>
        </#if>
        <#if realm.resetPasswordAllowed>
          <p class="mb-1">
            <span>
              <a href="${url.loginResetCredentialsUrl}">
                ${msg("doForgotPassword")}
              </a>
            </span>
          </p>
        </#if>
        <#if realm.password && realm.registrationAllowed && !usernameEditDisabled??>
          <p class="mb-0">
            <span>${msg("noAccount")}
              <a href="${url.registrationUrl}">
                ${msg("doRegister")}
              </a>
            </span>
          </p>
        </#if>
      </form>
    </#if>
  </#if>
</@layout.registrationLayout>
