# Software Heritage theme for Keycloak

This repository holds the development of a custom [Keycloak](https://www.keycloak.org/)
theme matching Software Heritage [web application](https://archive.softwareheritage.org)
design.

The theme support the following Keycloak components:

* Account Management {F3979207}

* Admin console {F3979209}

* Login related forms (based on [AdminLTE](https://adminlte.io/themes/dev/AdminLTE/index3.html)
and [bootstrap](https://getbootstrap.com/)).
They are freely inspired from the
[adminlte-keycloak-theme](https://github.com/MAXIMUS-DeltaWare/adminlte-keycloak-theme)
project.

  - user login {F3979210}

  - user registration {F3979211}

  - password reset {F3979212}

  - password update {F3979218}
